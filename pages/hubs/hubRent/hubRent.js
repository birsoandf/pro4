$(function() {
    $('#datetimepicker1').datetimepicker({
        language: 'pt-BR'
    });

});

function buttonRent() {
         setupUser();
        if($("#datetimepicker1").find("input").val() >= "12/28/2019 00:00 AM"){
            alert("INCHIRIAT PE DATA " + $("#datetimepicker1").find("input").val() );
        }
        else{
            alert("NU SE POATE INCHIRIA. Motiv: Data trecuta" + "12/28/2019 00:00 AM");
        }

}

function buttonReserve() {
    setupUser();
    if($("#datetimepicker1").find("input").val() >= "12/28/2019 00:00 AM"){
        alert("REZERVAT PE DATA " + $("#datetimepicker1").find("input").val() );
    }
    else{
        alert("NU SE POATE REZERVA. Motiv: Data trecuta" + "12/28/2019 00:00 AM");
    }

}

setupUser = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    if (isUserLogin !== 'true') {
        localStorage.setItem("loginRedirectURL", '../hubs/hubdetails/hubDetail.html');
        localStorage.setItem("typeOfUser", 'user');
        window.location.href = '../../login/login.html';
    }
};