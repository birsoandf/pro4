$(document).ready(() => {
    //alert("Hi");
    setDetails();
    fillStartReviews();
    locateMeButtons();
});

locateMeButtons = () => {
    const modal = document.getElementById("myModal");
    const span = $('#closeModal');
    const locateMe = $('#locateMe1');
    locateMe.click(()=>{
        modal.style.display = "block";
    });
    span.click(()=>{
        modal.style.display = "none";
    });
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
};



fillStartReviews = () => {
    const fivestartnumber = $(".side #fivestars");
    const fivestartnumberValue = localStorage.getItem("Bar5");
    console.log(fivestartnumberValue);
    fivestartnumber.text(fivestartnumberValue);
    console.log("Fill");
};

setDetails = () => {
    localStorage.setItem("Total", 354);
    localStorage.setItem("Bar1", 20);
    localStorage.setItem("Bar2", 6);
    localStorage.setItem("Bar3", 115);
    localStorage.setItem("Bar4", 63);
    localStorage.setItem("Bar5", 50);

    console.log("SetDet");
};

var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        expanded = false;
    }
}


function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
}