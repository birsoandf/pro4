$(document).ready(() => {
    setClickURLDirection();
    hubAdded();
});

hubAdded = () => {
    const location7 = $('.hubs_lis-class #location_7');
    const hasAdded = localStorage.getItem('hub7Added');
    if(hasAdded && hasAdded === '1'){
        location7.attr('class','column_new-line added');
    }else{
        location7.attr('class','column_new-line to_add b');
    }
};

setClickURLDirection = () => {
    const location1 = $('.hubs_lis-class #location_1');
    const location2 = $('.hubs_lis-class #location_2');
    const location3 = $('.hubs_lis-class #location_3');
    const location4 = $('.hubs_lis-class #location_4');
    const location5 = $('.hubs_lis-class #location_5');
    const location6 = $('.hubs_lis-class #location_6');
    const location7 = $('.hubs_lis-class #location_7');

    let location1URL = './hubdetails/hubDetail.html';
    let location2URL = './hubdetails/hubDetail2.html';
    let location3URL = './hubdetails/hubDetail3.html';
    let location4URL = './hubdetails/hubDetail4.html';
    let location5URL = './hubdetails/hubDetail5.html';
    let location6URL = './hubdetails/hubDetail6.html';
    let location7URL = './hubdetails/hubDetail7.html';

    const goToResourcesOfHub = localStorage.getItem("hubsRedirectURL") === '1';

    location1.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location1URL;
        }
    });
    location2.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location2URL;
        }
    });
    location3.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location3URL;
        }
    });
    location4.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location4URL;
        }
    });
    location5.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location5URL;
        }
    });
    location6.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location6URL;
        }
    });
    location7.click(() => {
        localStorage.setItem("hubsRedirectURL", '0');
        if (goToResourcesOfHub) {
            window.location = '../administration/resources/resources.html';
        } else {
            window.location = location7URL;
        }
    });
};


function sortList() {
    var list, i, switching, b1, shouldSwitch, c, j, b2;
    list = document.getElementById("hubs_lis");
    switching = true;
    c = list.getElementsByTagName("li");
    for (j = 0; j < c.length; j++) {
        c[j].style.clear = "none";
    }
    while (switching) {
        switching = false;
        c = list.getElementsByClassName("column");
        for (j = 0; j < (c.length - 1); j++) {
            b1 = c[j].getElementsByTagName("h2");
            b2 = c[j + 1].getElementsByTagName("h2");
            shouldSwitch = false;
            if (b1 !== null && b1.length > 0 && b2 !== null && b2.length > 0) {
                if (b1[0].innerHTML.toLowerCase() > b2[0].innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
            }
            if (shouldSwitch) {
                c[j].parentNode.insertBefore(c[j + 1], c[j]);
                switching = true;
            }
        }
    }
    for (j = 0; j < c.length; j += 3) {
        c[j].style.clear = "both";
    }
}

function sortListByAddress() {
    var list, i, switching, b1, shouldSwitch, c, j, b2;
    list = document.getElementById("hubs_lis");
    switching = true;
    c = list.getElementsByTagName("li");
    for (j = 0; j < c.length; j++) {
        c[j].style.clear = "none";
    }
    while (switching) {
        switching = false;
        c = list.getElementsByClassName("column");
        for (j = 0; j < (c.length - 1); j++) {
            b1 = c[j].getElementsByTagName("h3");
            b2 = c[j + 1].getElementsByTagName("h3");
            shouldSwitch = false;
            if (b1 !== null && b1.length > 0 && b2 !== null && b2.length > 0) {
                if (b1[0].innerHTML.toLowerCase() > b2[0].innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
            }
            if (shouldSwitch) {
                c[j].parentNode.insertBefore(c[j + 1], c[j]);
                switching = true;
            }
        }
    }
    for (j = 0; j < c.length; j += 3) {
        c[j].style.clear = "both";
    }
}

function sortListByStars() {
    var list, i, switching, b1, shouldSwitch, c, j, b2;
    list = document.getElementById("hubs_lis");
    switching = true;
    c = list.getElementsByTagName("li");
    for (j = 0; j < c.length; j++) {
        c[j].style.clear = "none";
    }
    while (switching) {
        switching = false;
        c = list.getElementsByClassName("column");
        for (j = 0; j < (c.length - 1); j++) {
            b1 = c[j].getElementsByClassName("checked");
            b2 = c[j + 1].getElementsByClassName("checked");
            shouldSwitch = false;
            if (b1 !== null && b2 !== null) {
                if (b2.length > b1.length) {
                    shouldSwitch = true;
                }
            }
            if (shouldSwitch) {
                c[j].parentNode.insertBefore(c[j + 1], c[j]);
                switching = true;
            }
        }
    }
    for (j = 0; j < c.length; j += 3) {
        c[j].style.clear = "both";
    }
}

function listView() {
    var list = document.getElementById("hubs_lis");
    var elements = document.getElementsByClassName("column");
    var i;
    for (i = 0; i < elements.length; i++) {
        elements[i].style.width = "80%";
    }
    list.style.marginLeft = "10%";
}

function gridView() {
    var list = document.getElementById("hubs_lis");
    var elements = document.getElementsByClassName("column");
    var i;
    for (i = 0; i < elements.length; i++) {
        elements[i].style.width = "30%";
    }
    list.style.marginLeft = "5%";
}