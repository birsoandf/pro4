$(document).ready(() => {
    const username = document.getElementById('username');
    const password = document.getElementById('password');
    const err = document.getElementById('err');
    const register = document.getElementById('register');
    const login = document.getElementById('login');
    err.hidden = true;
    login.onclick = () => this.login(username.value, password.value);
    register.onclick = () => this.redirectToRegister();
});

var login = (username, password) => {
    const users = JSON.parse(localStorage.getItem("users"));
    const err = document.getElementById('err');
    const URL = localStorage.getItem("loginRedirectURL");
    const type = localStorage.getItem("typeOfUser");
    users.forEach(user => {
        if (username === user.username && password === user.password && user.type === type) {
            localStorage.setItem('isUserLogin', 'true');
            localStorage.setItem('userLogin', JSON.stringify(user));
            window.location.href = URL;
            return false;
        }else if(user.type !== type){
            err.hidden = false;
            err.value = "User doesn't have a type!";
        } else {
            err.hidden = false;
            err.value = 'Invalid user!';
        }
    });
    return false;
};

var redirectToRegister = () =>{
    window.location.href = "../register/register.html"
};


