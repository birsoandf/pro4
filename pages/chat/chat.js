const msgerForm = document.getElementsByClassName("msger-inputarea");
const msgerInput = document.getElementsByClassName("msger-input");
const msgerChat = document.getElementsByClassName("msger-chat");

const BOT_MSGS = [
    //Salut
    "Salut, cu ce te putem ajuta?",
    //Am vazut ca limita la birouri este 26 desi noi am avea nevoie de 28
    "Sigur, se poate rezolva.",
    //Cand il gasesc pe manager online?
    "De lunea pana vinerea de la 16 la 20.",
    //Ok multumesc, salut
    "O saptamana productiva."
];

// Icons made by Freepik from www.flaticon.com
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "BOT";
const PERSON_NAME = "You";
var r = 0;

$(document).ready(function() {
    msgerForm[0].addEventListener("click", event => {
    event.preventDefault();

    const msgText = msgerInput[0].value;
    if (!msgText) return;

    appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText);
    msgerInput[0].value = "";

    botResponse();
}); });

function appendMessage(name, img, side, text) {
    //   Simple solution for small apps
    const msgHTML = `
    <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>

      <div class="msg-bubble">
        <div class="msg-info">
          <div class="msg-info-name">${name}</div>
          <div class="msg-info-time">${formatDate(new Date())}</div>
        </div>

        <div class="msg-text">${text}</div>
      </div>
    </div>
  `;

    msgerChat[0].insertAdjacentHTML("beforeend", msgHTML);
    msgerChat[0].scrollTop += 500;
}

function botResponse() {

    const msgText = BOT_MSGS[r];
    const delay = msgText.split(" ").length * 100;

    setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
    }, delay);
    r++;
}

// Utils
function get(selector, root = document) {
    return root.querySelector(selector);
}

function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}
