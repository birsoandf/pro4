$(document).ready(() => {
    setupUser();
    togglingButtons();
    updateDescription();
});

setupUser = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    if (isUserLogin !== 'true') {
        localStorage.setItem("loginRedirectURL", '../hubs/hubs.html');
        localStorage.setItem("hubsRedirectURL", '1');
        localStorage.setItem("typeOfUser", 'admin');
        window.location.href = '../../login/login.html';
    }
};

updateDescription = () => {
    const updateBtn = $('#updateDescription');
    updateBtn.click(() => {
        setTimeout(() => {
            updateBtn.text("SUCCESS!");
            setTimeout(() => {
                updateBtn.text("UPDATE DESCRIPTION!");
            }, 1000);
        }, 250);
    })
};

togglingButtons = () => {
    let waterBool = true;
    let electBool = true;
    let elect1Bool = true;
    let elect2Bool = true;
    let elect3Bool = true;
    let gasBool = true;
    let fansBool = true;
    let fans1Bool = true;
    let fans2Bool = true;
    let fans3Bool = true;

    const waterI = $('.resourcessss #waterUseI');
    const electI = $('.resourcessss #electUseI');
    const elect1I = $('.resourcessss #elect1stUseI');
    const elect2I = $('.resourcessss #elect2ndUseI');
    const elect3I = $('.resourcessss #elect3rdUseI');
    const gasI = $('.resourcessss #gasUseI');
    const fansI = $('.resourcessss #fansUseI');
    const fans1I = $('.resourcessss #fans1stUseI');
    const fans2I = $('.resourcessss #fans2ndUseI');
    const fans3I = $('.resourcessss #fans3rdUseI');

    const water = $('.resourcessss #waterUse');
    const elect = $('.resourcessss #electUse');
    const elect1 = $('.resourcessss #elect1stUse');
    const elect2 = $('.resourcessss #elect2ndUse');
    const elect3 = $('.resourcessss #elect3rdUse');
    const gas = $('.resourcessss #gasUse');
    const fans = $('.resourcessss #fansUse');
    const fans1 = $('.resourcessss #fans1stUse');
    const fans2 = $('.resourcessss #fans2ndUse');
    const fans3 = $('.resourcessss #fans3rdUse');

    waterI.click(() => {
        if (waterBool) {
            setTimeout(() => {
                waterBool = !waterBool;
                water.attr('value', 0);
            }, 1200);
        } else {
            setTimeout(() => {
                waterBool = !waterBool;
                water.attr('value', 200);
            }, 1200);
        }
    });

    electI.click(() => {
        if (electBool) {
            setTimeout(() => {
                electBool = !electBool;
                elect.attr('value', 0);
                elect1.attr('value', 0);
                elect2.attr('value', 0);
                elect3.attr('value', 0);
            }, 1200);
        } else {
            setTimeout(() => {
                electBool = !electBool;
                elect.attr('value', 300);
                elect1.attr('value', 100);
                elect2.attr('value', 150);
                elect3.attr('value', 50);
            }, 1200);
        }
    });

    elect1I.click(() => {
        if (elect1Bool) {
            setTimeout(() => {
                elect1Bool = !elect1Bool;
                elect1.attr('value', 0);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                elect1Bool = !elect1Bool;
                elect1.attr('value', 100);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        }
    });

    elect2I.click(() => {
        if (elect2Bool) {
            setTimeout(() => {
                elect2Bool = !elect2Bool;
                elect2.attr('value', 0);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                elect2Bool = !elect2Bool;
                elect2.attr('value', 150);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        }
    });

    elect3I.click(() => {
        if (elect3Bool) {
            setTimeout(() => {
                elect3Bool = !elect3Bool;
                elect3.attr('value', 0);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                elect3Bool = !elect3Bool;
                elect3.attr('value', 50);
                const sum = parseInt(elect1.attr('value'), 10) + parseInt(elect2.attr('value'), 10) + parseInt(elect3.attr('value'), 10);
                elect.attr('value', sum);
            }, 1200);
        }
    });

    gasI.click(() => {
        if (gasBool) {
            setTimeout(() => {
                gasBool = !gasBool;
                gas.attr('value', 0);
            }, 1200);
        } else {
            setTimeout(() => {
                gasBool = !gasBool;
                gas.attr('value', 50);
            }, 1200);
        }
    });

    fansI.click(() => {
        if (fansBool) {
            setTimeout(() => {
                fansBool = !fansBool;
                fans.attr('value', 0);
                fans1.attr('value', 0);
                fans2.attr('value', 0);
                fans3.attr('value', 0);
            }, 1200);
        } else {
            setTimeout(() => {
                fansBool = !fansBool;
                fans.attr('value', 50);
                fans1.attr('value', 10);
                fans2.attr('value', 15);
                fans3.attr('value', 25);
            }, 1200);
        }
    });

    fans1I.click(() => {
        if (fans1Bool) {
            setTimeout(() => {
                fans1Bool = !fans1Bool;
                fans1.attr('value', 0);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                fans1Bool = !fans1Bool;
                fans1.attr('value', 10);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        }
    });

    fans2I.click(() => {
        if (fans2Bool) {
            setTimeout(() => {
                fans2Bool = !fans2Bool;
                fans2.attr('value', 0);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                fans2Bool = !fans2Bool;
                fans2.attr('value', 15);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        }
    });

    fans3I.click(() => {
        if (fans3Bool) {
            setTimeout(() => {
                fans3Bool = !fans3Bool;
                fans3.attr('value', 0);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        } else {
            setTimeout(() => {
                fans3Bool = !fans3Bool;
                fans3.attr('value', 25);
                const sum = parseInt(fans1.attr('value'), 10) + parseInt(fans2.attr('value'), 10) + parseInt(fans3.attr('value'), 10);
                fans.attr('value', sum);
            }, 1200);
        }
    });
};