$(document).ready(() => {
    setupUser();
    const addhubBtn = document.getElementById("addHubss");
    addhubBtn.onclick = () => this.addHub();
});

setupUser = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    if (isUserLogin !== 'true') {
        localStorage.setItem("loginRedirectURL", '../administration/addhubs/addhubs.html');
        localStorage.setItem("typeOfUser", 'admin');
        window.location.href = '../../login/login.html';
    }
};

addHub = () => {
    const hubname = $('.input-type #hubname').val();
    const hublocation = $('.input-type #hublocation').val();
    const hubemail = $('.input-type #hubemail').val();
    const hubphone = $('.input-type #hubphone').val();
    const hubcity = $('.input-type #hubcity').val();
    const hubcountry = $('.input-type #hubcountry').val();
    const errr = $('#errHub');
    const location7 = $('.hubs_lis-class #location_7');

    if (hubname.length === 0 || hubphone.length === 0 || hublocation.length === 0 || hubemail.length === 0 || hubcity.length === 0 || hubcountry.length === 0) {
        errr.attr("value", "Fields must not be empty");
        errr.attr("class", "input-style errorAddHub");
    } else {
        const hubsList = JSON.parse(localStorage.getItem("hubList"));
        errr.attr("value", "Add hub with success!");
        errr.attr("class", "input-style successAddHub");
        let hub = {
            name: hubname,
            location: hublocation,
            email: hubemail,
            phone: hubphone
        };
        hubsList.push(hub);
        localStorage.setItem("hubList",JSON.stringify(hubsList));
        localStorage.setItem("hub7Added",'1');
        // location7.attr('class', "column_new-line added");
        return false;
    }
    return false;
};