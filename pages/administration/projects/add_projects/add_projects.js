$(document).ready(() => {
    setupUser();
});

setupUser = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    if (isUserLogin !== 'true') {
        localStorage.setItem("loginRedirectURL", '../administration/projects/add_projects/add_projects.html');
        localStorage.setItem("typeOfUser", 'user');
        window.location.href = '../../../login/login.html';
    }
};

function addProject() {
    var x = localStorage.getItem("hidden_project");
    if (x === null || x === "hidden") {
        localStorage.setItem("hidden_project", "visible");
    } else {
        localStorage.setItem("hidden_project", "hidden");
    }
}