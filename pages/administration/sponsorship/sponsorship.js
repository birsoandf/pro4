$(document).ready(() => {
    setupUser();
});

setupUser = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    if (isUserLogin !== 'true') {
        localStorage.setItem("loginRedirectURL", '../administration/projects/project_details/project_details.html');
        localStorage.setItem("typeOfUser", 'admin');
        window.location.href = '../../login/login.html';
    }
};

function checkToShowProject() {
    var x = localStorage.getItem("hidden_project");
    if (x === "visible") {
        document.getElementById("hidden_project").style.visibility = "visible";
    }
}