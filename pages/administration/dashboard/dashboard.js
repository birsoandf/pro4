window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "dark1", // "light1", "light2", "dark1", "dark2"
        title:{
            text: "Top searched resources"
        },
        axisY: {
            title: "Reserves(bnow)"
        },
        data: [{        
            type: "column",  
            showInLegend: true, 
            legendMarkerColor: "grey",
            legendText: "bnow = by number of words",
            dataPoints: [      
                { y: 4808, label: "Coffee machine"},
                { y: 4301, label: "Computer" },
                { y: 3822,  label: "TVs" },
                { y: 3773,  label: "Fans" },
                { y: 3315,  label: "Seats" },
                { y: 2888,  label: "Printer" },
                { y: 2737, label: "Comfy chairs" },
                { y: 2390,  label: "Green energy" }
            ]
        }]
    });
    chart.render();
    
    document.getElementById("example_a").addEventListener("click",function(){
    	chart.exportChart({format: "jpg"});
        chart2.exportChart({format: "jpg"});
        chart3.exportChart({format:"jpg"});
        chart4.exportChart({format:"jpg"});


    });  
    

    var chart2 = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        theme: "dark1",
        title: {
            text: "Active users by hour"
        },
        data: [{
            type: "pie",
            startAngle: 240,
            yValueFormatString: "##0.00\"%\"",
            indexLabel: "{label} {y}",
            dataPoints: [
                {y: 4.85, label: "8AM-10AM"},
                {y: 13.31, label: "10AM-12PM"},
                {y: 20.16, label: "12PM-2PM"},
                {y: 23.12, label: "2PM-4PM"},
                {y: 38.26, label: "4PM-12AM"},
                {y: 5.3, label: "12AM-8AM"}
            ]
        }]
    });
    chart2.render();

    var chart3 = new CanvasJS.Chart("chartContainer3", {
        theme: "dark2", // "light1", "light2", "dark1", "dark2"
        animationEnabled: true,
        title: {
            text: "Interest rate by available seats"
        },
        axisY: {
            title: "No. of seats",
            includeZero: false
        },
        data: [{
            type: "rangeColumn",
            yValueFormatString: "#,##0.00",
            toolTipContent: "{x}<br>Low: {y[0]}<br>High: {y[1]}",
            dataPoints: [		
                { x: 10, y: [27.10, 48.99] },
                { x: 20, y: [35.95, 62.54] },
                { x: 30, y: [37.27, 78.50] },
                { x: 40, y: [43.33, 50.51] },
                { x: 50, y: [46.69, 52.86] },
                { x: 60, y: [31.80, 40.75] },
                { x: 70, y: [31.51, 41.22] },
                { x: 80, y: [35.09, 40.14] },
                { x: 90, y: [17.98, 33.73] },
                { x: 100, y: [13.57, 30.49] },
            ]
        }]
    });
    chart3.render();

    var chart4 = new CanvasJS.Chart("chartContainer4", {
        theme:"dark1",
        animationEnabled: true,
        title:{
            text: "Total revenue by number of resources and seats"
        },
        axisX: {
            title:"No. of seats"
        },
        axisY: {
            title:"No. of resources"
        },
        legend:{
            horizontalAlign: "left"
        },
        data: [{
            type: "bubble",
            showInLegend: true,
            legendText: "---------Bubble Represents Money in Thousands/Month",
            legendMarkerType: "circle",
            legendMarkerColor: "grey",
            toolTipContent: "No. of seats: {x}<br/> No. of resources: {y}<br/> Revenue: {z} thous.",
            dataPoints: [
                //{ x: 68.3, y: 2.4, z: 1309.05 , name: "India"},
                //{ x: 76, y: 1.57, z:1371.22, name: "China"},
                    { x: 78.7, y: 1.84, z:320.896},
                { x: 69.1, y: 2.44, z: 258.162},
                { x: 74.7, y: 1.78, z: 225.962 },
                { x: 76.9, y: 2.21, z: 125.890 },
                { x: 53, y: 5.59, z: 181.181 },
                { x: 70.9, y: 1.75, z: 144.096 },
                { x: 83.8, y: 1.46, z:127.141 },
                { x: 82.5, y: 1.83, z:23.789 },
                { x: 71.3, y: 3.31, z: 93.778 },
                { x: 81.6, y: 1.81, z:65.128 },
                { x: 62.1, y: 4.26, z: 47.236 },
                { x: 69.6, y: 4.51, z: 36.115 },
                { x: 60.7, y: 4.65, z: 33.736 },
                { x: 52.7, y: 6, z: 27.859 },
                { x: 68.4, y: 2.94, z: 101.716 },
                { x: 70, y: 2.17, z: 28.656 },
                { x: 71.2, y: 1.51, z: 45.154 },
                { x: 83.4, y: 1.62, z: 46.447 },
                { x: 64.6, y: 4.28, z: 99.873 },
                { x: 74.6, y: 1.5, z: 68.65 },
                { x: 74.2, y: 1.88, z: 48.228 },
                { x: 74.44, y: 2.34, z: 31.155},
                { x: 57.4, y: 2.34, z: 55 },
                { x: 59.2, y: 3.86, z: 15.77},
                { x: 55.9, y: 4.63, z: 22.834}
            ]
        }]
    });
    chart4.render();
}
