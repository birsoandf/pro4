$(document).ready(() => {
    initDataOfTheApp();
    setTitle(localStorage.getItem("LogoName"));
    logoutUser();
    showSlides();
    selectForResourcesHub();
});

setTitle = (title) => {
    $('.header #logoApp').text(title);
};

selectForResourcesHub = () => {
    const isUserLogin = localStorage.getItem('isUserLogin');
    const res = $('.header-right .dropdown .dropdown-content #resourcesHead');
    if (isUserLogin === 'true') {
        res.click(function () {
            localStorage.setItem("hubsRedirectURL", '1');
            window.location = 'http://localhost:63342/pro4/pages/hubs/hubs.html';
            return false;
        });
    }
};

initDataOfTheApp = () => {
    const storeStatus = localStorage.getItem("storeState");
    if (storeStatus !== '1') {
        //data zone
        const LOGONAME = 'BestT4';
        const users = [];
        users.push({username: 'anab', password: '123', type: 'admin'});
        users.push({username: 'mihaib', password: '123', type: 'user'});
        const hubs = [];
        hubs.push({
            name: 'DECALOG',
            location: 'Str. Primaverii, nr.18',
            phone: '0765738234',
            email: "a@gmail.com",
            country: "France",
            city: 'Paris'
        });

        //put it on localStorage to be persistent and all other components to have access to it
        localStorage.setItem("LogoName", LOGONAME);
        localStorage.setItem("users", JSON.stringify(users));
        localStorage.setItem("storeState", '1');
        localStorage.setItem("hubList", JSON.stringify(hubs));
        localStorage.setItem("hubsRedirectURL", '0');
    }
};
var slideIndex = 0;
logoutUser = () => {
    const loginBtn = $('.header-right #loginHead');
    const isUserLogin = localStorage.getItem("isUserLogin");
    if (isUserLogin === 'true') {
        loginBtn.text('Logout');
        loginBtn.click(function () {
            localStorage.setItem("isUserLogin", "false");
            localStorage.setItem("userLogin", "");
            localStorage.setItem("typeOfUser", "");
            loginBtn.text('Login');
        });
    }
};

var showSlides = () => {
    let i;
    const slides = document.getElementsByClassName("mySlides");
    const dots = document.getElementsByClassName("dot");
    if (slides && dots) {
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        if (slides[slideIndex - 1] && dots[slideIndex - 1]) {
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
        setTimeout(showSlides, 8500);
    }
};

currentSlide = (n) => {
    showSlides(slideIndex = n);
};